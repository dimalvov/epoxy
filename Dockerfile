FROM continuumio/miniconda3

RUN apt-get update && apt-get install -y \
 libpq-dev \
 build-essential \
&& rm -rf /var/lib/apt/lists/*

ADD . /app
WORKDIR /app

RUN [ "conda", "env", "create" ]

ENV FLASK_DEBUG 0

ENTRYPOINT [ "./run.sh" ]
