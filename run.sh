#!/bin/bash

export FLASK_APP=app.py
source activate epoxy
python create_mongo_user.py
cd epoxy
echo "Migrations..."
python -m flask migration_up
echo "Run..."
python ./app.py --host=0.0.0.0 --port=80
