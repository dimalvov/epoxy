import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

DEBUG = True

MONGODB_SETTINGS = {
    'host': 'mongo',
    'db': 'epoxy',
    'username': os.environ.get('MONGO_EPOXY_USERNAME', None),
    'password': os.environ.get('MONGO_EPOXY_PASSWORD', None)
}

SECRET_KEY = "9oup6z5mdbw)8(f5$9ob@m&xha*(5ulqot&x*y1n$1^^9qo#d-"

# Flask-Security config
SECURITY_URL_PREFIX = "/admin"
SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
SECURITY_PASSWORD_SALT = "ATGUOHAELKiubahiughaerGOJAEGj"

# Flask-Security URLs, overridden because they don't put a / at the end
SECURITY_LOGIN_URL = "/login/"
SECURITY_LOGOUT_URL = "/logout/"

SECURITY_POST_LOGIN_VIEW = "/admin/"
SECURITY_POST_LOGOUT_VIEW = "/admin/"

# Flask-Security features
SECURITY_REGISTERABLE = False
SECURITY_CONFIRMABLE = False
SECURITY_SEND_REGISTER_EMAIL = False
SECURITY_RECOVERABLE = True

USER_IDENTITY_ATTRIBUTES = ['email']

MAIL_SERVER = "mail"
MAIL_PORT = 1025
MAIL_DEFAULT_SENDER = "noreply@epistatica.eu"
