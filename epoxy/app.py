import werkzeug
import json
import io
import re
import os
import tempfile
import flask_admin
import structlog
import requests

from functools import wraps

from flask import Flask, Response, request, jsonify, url_for, render_template, g
from flask_admin import helpers as admin_helpers
from flask_mongoengine import MongoEngine
from flask_httpauth import HTTPTokenAuth
from flask_mail import Mail

from collections import OrderedDict, Iterable
from flask_security import Security, MongoEngineUserDatastore, \
    login_required, current_user
from flask_mongoengine_migrations import FlaskMigrations
from mongoengine.queryset.visitor import Q
from werkzeug.exceptions import BadRequest, BadGateway

from flaskrun import flaskrun
from models import *
from views import *

###
# test example:
#
# curl -X POST -H "Authorization: Bearer e83635ff69e0d41f989368b11066d772" \
#   -H "Content-type: application/json" \
#   -d '{ "Inputs": { "input1": [ { "input_1": "", "input_2": "", "input_3": "", "input_4": "", "input_5": "", "input_6": "", "input_7": "", "input_8": "", "input_9": "", "input_10": "", "input_11": "", "input_12": "", "input_13": "", "input_14": "", "input_15": "", "input_16": "", "input_17": "" } ] }, "GlobalParameters": { } }' \
#   http://localhost:5000/endpoint/?name=model1
###


structlog.configure(
    processors=[
        structlog.processors.KeyValueRenderer(
            key_order=['event', 'request_id'],
        ),
    ],
    context_class=structlog.threadlocal.wrap_dict(dict),
    logger_factory=structlog.stdlib.LoggerFactory(),
)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__, instance_relative_config=False)
app.config.from_object('config')

auth = HTTPTokenAuth(scheme='Bearer')
db = MongoEngine(app)
migrate = FlaskMigrations(app)
user_datastore = MongoEngineUserDatastore(db, User, Role)
mail = Mail(app)
security = Security(app, user_datastore)

admin = flask_admin.Admin(
    app,
    'Epoxy',
    base_template='master.html',
    template_mode='bootstrap3',
)

admin.add_view(AdminView(Role, name='Roles'))
admin.add_view(UserAdminView(User, name='Users'))
admin.add_view(EndpointView(Endpoint, name='Endpoints'))
admin.add_view(CounterView(Counter, name='Billing'))

@auth.verify_token
def verify_token(token):
    name = request.args.get('name')

    endpoint = Endpoint.objects(Q(name=name) & Q(enabled__ne=False)).first()
    if not endpoint:
        return False

    superuser = Role.objects(name='superuser').first()

    g.user = User.objects(Q(apikey=token) & Q(active=True) & Q(roles__nin=[superuser.id]) & Q(endpoints__in=[endpoint.id])).first()

    return True if g.user else False

@app.errorhandler(404)
def not_found(error):
    return jsonify(dict(message="Resource doesn't exist."))

@app.errorhandler(500)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/')
def index():
    return render_template('index.html')

@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )

@app.route('/endpoint/', methods=['POST'])
@auth.login_required
def call_model():
    if request.headers.get('Content-Type') != 'application/json':
        raise BadRequest('Only application/json please')

    if not request.json:
        raise BadRequest('Empty or malformed body')

    name = request.args.get('name')
    endpoint = Endpoint.objects(name=name).first()

    headers = {
        'Content-Type': 'application/json',
        'Authorization': ('Bearer ' + endpoint.backend_apikey)
    }

    r = requests.post(endpoint.backend_url, headers=headers, data=request.data)
    backend_json = r.json()

    h = History(
        user=g.user,
        endpoint=endpoint,
        status_code=r.status_code,
        request=request.json,
        response=backend_json
    )

    h.save()

    ym = datetime.datetime.utcnow().strftime('%Y%m')
    inc = 1 if r.status_code == 200 else 0

    Counter.objects(
        user=g.user,
        endpoint=endpoint,
        ym=ym).update_one(
            upsert=True,
            inc__success_count=inc,
            inc__fail_count=inc
        )

    if not 'Results' in backend_json \
            or not 'output1' in backend_json['Results'] \
            or len(backend_json['Results']['output1']) != 1:
        raise BadGateway(backend_json['error']['message'])

    res = backend_json['Results']['output1'][0]

    print(res)

    return Response(
        # response=json.dumps(dict(zip(backend_json['Results']['output1']['Value']['ColumnNames'], backend_json['Results']['output1']['Value']['Values'][0]))),
        response=json.dumps(res),
        status=r.status_code,
        mimetype="application/json"
    )

if __name__ == "__main__":
    flaskrun(app)
