from models import User, Role
from flask_security import MongoEngineUserDatastore

def up(db):
    user_datastore = MongoEngineUserDatastore(db, User, Role)
    user_datastore.create_role(name='superuser')
    user_datastore.create_role(name='user')

    user_datastore.create_user(
        email='admin@example.com',
        password='admin',
        apikey='',
        roles=['superuser']
    )

def down(db):
    db['user'].drop()
    db['role'].drop()
