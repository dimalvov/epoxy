from flask_security import current_user, utils
from flask import url_for, redirect, render_template, request, abort, Markup, flash, Response
from flask_admin.base import expose
from flask_admin.contrib.mongoengine import ModelView
from flask_admin.actions import action
from flask_admin.model import typefmt
from flask_admin.model.template import macro
from wtforms.fields import PasswordField, StringField
from wtforms.widgets import Input
from models import Endpoint, User
from datetime import date
from mongoengine.queryset.visitor import Q

MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
        type(None): typefmt.null_formatter,
        date: lambda view, value: value.strftime("%Y-%m-%d %H:%M")
    })


class AdminView(ModelView):
    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('superuser'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                abort(403)
            else:
                return redirect(url_for('security.login', next=request.url))


class ApikeyField(StringField):
    def __call__(self, **kwargs):
        return super(self.__class__, self).__call__(**kwargs) + "<a href='#' onclick='javascript:$(this).prev(\"input\").val(Array.apply(0, Array(32)).map(function() { return Math.floor(Math.random() * 16).toString(16); }).join(\"\"));'>Regenerate</a>"


class UserAdminView(AdminView):
    form_overrides = dict(apikey=ApikeyField)
    column_exclude_list = list = ('password',)
    form_excluded_columns = ('password',)

    def scaffold_form(self):
        form_class = super(self.__class__, self).scaffold_form()
        form_class.password2 = PasswordField('New Password')
        return form_class

    def on_model_change(self, form, model, is_created):
        if len(model.password2):
            model.password = utils.encrypt_password(model.password2)


class EndpointView(AdminView):
    column_type_formatters = MY_DEFAULT_FORMATTERS
    can_view_details = True
    column_exclude_list = ('backend_url', 'backend_apikey')

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        return True

    def get_query(self, *args, **kwargs):
        if current_user.has_role('superuser'):
            return super(EndpointView, self).get_query(*args, **kwargs)

        user = User.objects.get(id=current_user.id)

        ft = Q(enabled__ne=False) & Q(id__in=[e.id for e in user.endpoints])

        return super(EndpointView, self).get_query(*args, **kwargs).filter(ft)

    @property
    def can_edit(self):
        return current_user.has_role('superuser')

    @property
    def can_create(self):
        return current_user.has_role('superuser')

    @property
    def can_delete(self):
        return current_user.has_role('superuser')

    def example_formatter(view, context, model, name):
        return Markup("<pre>{example}</pre>".format(example=model.example))

    column_formatters = {
       'example': example_formatter
    }

class CounterView(AdminView):
    can_create = False
    can_edit = False
    can_delete = False
    can_view_details = False

    column_type_formatters = MY_DEFAULT_FORMATTERS

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        return True

    def get_query(self, *args, **kwargs):
        if current_user.has_role('superuser'):
            return super(CounterView, self).get_query(*args, **kwargs)

        return super(CounterView, self).get_query(*args, **kwargs).filter(user=current_user.id)
