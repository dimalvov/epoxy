import datetime
from mongoengine import *
from flask_security import Security, \
    UserMixin, RoleMixin, login_required, current_user

class Endpoint(Document):
    name = StringField(regex=r'^[a-z1-9_]+$', max_length=80, required=True, unique=True)
    enabled = BooleanField(default=True)
    backend_url = StringField(max_length=255, required=True)
    backend_apikey = StringField(max_length=255, required=True)
    example = StringField()

    def __str__(self):
        return self.name

class Role(Document, RoleMixin):
    name = StringField(max_length=80, unique=True)

    def __str__(self):
        return self.name

class User(Document, UserMixin):
    active = BooleanField(default=True)
    email = StringField(max_length=255, required=True, unique=True)
    password = StringField(max_length=255)
    apikey = StringField(max_length=80, required=True, unique=True)
    roles = ListField(ReferenceField(Role), default=[])
    endpoints = ListField(ReferenceField(Endpoint), default=[])

    def __str__(self):
        return self.email

class History(Document):
    created = DateTimeField(default=datetime.datetime.now)
    user = ReferenceField(User, required=True)
    endpoint = ReferenceField(Endpoint, required=True)
    status_code = IntField(required=True)
    request = DictField()
    response = DictField()

class Counter(Document):
    meta = {
        'indexes': [
            {'fields': ('user', 'endpoint', 'ym'), 'unique': True}
        ]
    }

    user = ReferenceField(User, required=True)
    endpoint = ReferenceField(Endpoint, required=True)
    ym = IntField(required=True)
    success_count = IntField(default=0)
    fail_count = IntField(default=0)
