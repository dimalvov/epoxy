Fresh install

* install docker
* git clone epoxy
* cd epoxy
* docker-compose up

Changes to web

* cd epoxy
* git pull
* docker ps -> find id and image of the web container
* docker stop {id_of_web_cont}
* docker rmi {image_of_web_cont}
* docker-compose up


To setup auth in mongo create a file named .env in project root:

```
MONGO_INITDB_ROOT_USERNAME=[admin username]
MONGO_INITDB_ROOT_PASSWORD=[admin password]
MONGO_EPOXY_USERNAME=[admin username]
MONGO_EPOXY_PASSWORD=[admin password]
```

Note that once mongo container created, these values won't have effect.
