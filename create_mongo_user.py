import os

from pymongo import MongoClient

client = MongoClient('mongo:27017')
client.admin.authenticate(os.environ['MONGO_INITDB_ROOT_USERNAME'], os.environ['MONGO_INITDB_ROOT_PASSWORD'])

client.epoxy.add_user(os.environ['MONGO_EPOXY_USERNAME'], os.environ['MONGO_EPOXY_PASSWORD'], roles=[{'role': 'readWrite', 'db': 'epoxy'}])

print("User created")
